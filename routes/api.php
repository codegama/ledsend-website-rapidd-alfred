<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('user', function (Request $request) {
    return $request->user();
});

Route::get('/email/verify', 'ApplicationController@email_verify')->name('email.verify');

Route::group(['prefix' => 'user' , 'middleware' => 'cors'], function() {

    Route::any('pages_list' , 'ApplicationController@static_pages_api');

    Route::get('get_settings_json', function () {

        if(\File::isDirectory(public_path(SETTINGS_JSON))){

        } else {

            \File::makeDirectory(public_path('default-json'), 0777, true, true);

            \App\Helpers\Helper::settings_generate_json();
        }

        $jsonString = file_get_contents(public_path(SETTINGS_JSON));

        $data = json_decode($jsonString, true);

        return $data;
    
    });

	/***
	 *
	 * User Account releated routs
	 *
	 */

    Route::post('register','Api\AccountApiController@register');
    
    Route::post('login','Api\AccountApiController@login');

    Route::post('forgot_password', 'Api\AccountApiController@forgot_password');

    Route::post('regenerate_email_verification_code', 'Api\AccountApiController@regenerate_email_verification_code');

    Route::post('verify_email', 'Api\AccountApiController@verify_email');

    Route::group(['middleware' => 'UserApiVal'] , function() {

        Route::post('dashboard', 'Api\CommonApiController@dashboard');

        Route::post('profile','Api\AccountApiController@profile'); // 1

        Route::post('update_profile', 'Api\AccountApiController@update_profile'); // 2

        Route::post('change_password', 'Api\AccountApiController@change_password'); // 3

        Route::post('delete_account', 'Api\AccountApiController@delete_account'); // 4

        Route::post('logout', 'Api\AccountApiController@logout'); // 7

        Route::post('notifications_status_update', 'Api\AccountApiController@notifications_status_update');  // 5

        // CARDS curd Operations

        Route::post('cards_add', 'Api\AccountApiController@cards_add'); // 15

        Route::post('cards_list', 'Api\AccountApiController@cards_list'); // 16

        Route::post('cards_delete', 'Api\AccountApiController@cards_delete'); // 17

        Route::post('cards_default', 'Api\AccountApiController@cards_default'); // 18

        Route::post('payment_mode_default', 'Api\AccountApiController@payment_mode_default');

        Route::post('users_search','Api\AccountApiController@users_search');

    });

    Route::post('project/configurations', 'ApplicationController@configurations'); 

    Route::group(['middleware' => 'UserApiVal'] , function() {

        Route::post('kyc_documents_list', 'Api\VerificationApiController@kyc_documents_list');

        Route::post('kyc_documents_save','Api\VerificationApiController@kyc_documents_save');

        Route::post('kyc_documents_delete','Api\VerificationApiController@kyc_documents_delete');

        Route::post('kyc_documents_delete_all','Api\VerificationApiController@kyc_documents_delete_all');

        Route::post('kyc_status_user','Api\VerificationApiController@kyc_status_user');

        Route::post('users_accounts_list','Api\VerificationApiController@users_accounts_list');

        Route::post('users_accounts_save','Api\VerificationApiController@users_accounts_save');

        Route::post('users_accounts_delete','Api\VerificationApiController@users_accounts_delete');
        
        Route::post('users_accounts_default','Api\VerificationApiController@users_accounts_default');

    });

    Route::post('admin_account_details','Api\WalletApiController@admin_account_details');

    Route::group(['middleware' => ['UserApiVal', 'KycVerified']], function() {

        Route::post('wallets_index','Api\WalletApiController@user_wallets_index');

        Route::post('wallets_add_money_by_stripe', 'Api\WalletApiController@user_wallets_add_money_by_stripe');

        Route::post('wallets_add_money_by_bank_account','Api\WalletApiController@user_wallets_add_money_by_bank_account');
        
        Route::post('wallets_add_money_by_razorpay','Api\WalletApiController@user_wallets_add_money_by_razorpay');
        
        Route::post('wallets_history','Api\WalletApiController@user_wallets_history');

        Route::post('wallets_history_for_add','Api\WalletApiController@user_wallets_history_for_add');

        Route::post('wallets_history_for_sent','Api\WalletApiController@user_wallets_history_for_sent');

        Route::post('wallets_history_for_received','Api\WalletApiController@user_wallets_history_for_received');

        Route::post('wallets_payment_view','Api\WalletApiController@user_wallets_payment_view');

        Route::post('wallets_send_money','Api\WalletApiController@user_wallets_send_money');

        // Withdrawls start

        Route::post('withdrawals_index','Api\WalletApiController@user_withdrawals_index');
        
        Route::post('withdrawals_view','Api\WalletApiController@user_withdrawals_view');

        Route::post('withdrawals_search','Api\WalletApiController@user_withdrawals_search');

        Route::post('withdrawals_send_request','Api\WalletApiController@user_withdrawals_send_request');

        Route::post('withdrawals_cancel_request','Api\WalletApiController@user_withdrawals_cancel_request');

        Route::post('withdrawals_check','Api\WalletApiController@user_withdrawals_check');

    });

    Route::group(['middleware' => ['UserApiVal', 'KycVerified']], function() {

        Route::post('generated_invoices_index', 'Api\InvoiceApiController@generated_invoices_index');

        Route::post('generated_invoices_view', 'Api\InvoiceApiController@generated_invoices_view');

        Route::post('generated_invoices_drafts', 'Api\InvoiceApiController@generated_invoices_drafts');

        Route::post('generated_invoices_scheduled', 'Api\InvoiceApiController@generated_invoices_scheduled');

        Route::post('generated_invoices_unpaid', 'Api\InvoiceApiController@generated_invoices_unpaid');

        Route::post('generated_invoices_paid', 'Api\InvoiceApiController@generated_invoices_paid');
        
        Route::post('generated_invoices_received', 'Api\InvoiceApiController@generated_invoices_received');

        Route::post('generated_invoices_draft', 'Api\InvoiceApiController@generated_invoices_draft');

        Route::post('generated_invoices_items', 'Api\InvoiceApiController@generated_invoices_items');

        Route::post('generated_invoices_items_save', 'Api\InvoiceApiController@generated_invoices_items_save');

        Route::post('generated_invoices_items_delete', 'Api\InvoiceApiController@generated_invoices_items_delete');

        Route::post('generated_invoices_info_save', 'Api\InvoiceApiController@generated_invoices_info_save');

        Route::post('generated_invoices_send', 'Api\InvoiceApiController@generated_invoices_send');

        Route::post('invoices_business_information', 'Api\InvoiceApiController@invoices_business_information');
        
        Route::post('generated_invoices_payment', 'Api\InvoiceApiController@generated_invoices_payment');

    });
    
    Route::group(['middleware' => 'UserApiVal'] , function() {

        Route::post('disputes_index', 'Api\InvoiceApiController@user_disputes_index');

        Route::post('disputes_opened', 'Api\InvoiceApiController@user_disputes_opened');

        Route::post('disputes_closed', 'Api\InvoiceApiController@user_disputes_closed');

        Route::post('disputes_view', 'Api\InvoiceApiController@user_disputes_view');

        Route::post('disputes_send', 'Api\InvoiceApiController@user_disputes_send');

        Route::post('disputes_cancel', 'Api\InvoiceApiController@user_disputes_cancel');

        Route::post('disputes_messages', 'Api\InvoiceApiController@user_disputes_messages');
        
        Route::post('disputes_messages_save', 'Api\InvoiceApiController@user_disputes_messages_save');
    });

    Route::group(['middleware' => 'UserApiVal'] , function() {

        Route::post('razorpay_create', 'Api\AccountApiController@razorpay_create');
    });

});

