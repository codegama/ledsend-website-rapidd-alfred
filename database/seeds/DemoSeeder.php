<?php

use Illuminate\Database\Seeder;

use App\Helpers\Helper;

class DemoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(Schema::hasTable('admins')) {

            $check_admin_details = DB::table('admins')->where('email' , 'admin@ledsend.com')->count();

            if(!$check_admin_details) {

            	DB::table('admins')->insert([
            		[
        		        'name' => 'Admin',
                        'unique_id' => 'admin-demo',
        		        'email' => 'admin@ledsend.com',
                        'about' => 'About',
        		        'password' => \Hash::make('123456'),
        		        'picture' => envfile('APP_URL')."/placeholder.jpg",
                        'status' => 1,
        		        'created_at' => date('Y-m-d H:i:s'),
        		        'updated_at' => date('Y-m-d H:i:s')
        		    ]
                ]);

            }

            $check_test_admin_details = DB::table('admins')->where('email' , 'test@ledsend.com')->count();

            if(!$check_test_admin_details) {

                DB::table('admins')->insert([

                    [
                        'name' => 'Test',
                        'unique_id' => 'admin-demo',
                        'email' => 'test@ledsend.com',
                        'password' => \Hash::make('123456'),
                        'about' => 'About',
                        'picture' => envfile('APP_URL')."/placeholder.jpg",
                        'status' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ],
    		    ]);
            }
        
        }

        if(Schema::hasTable('users')) {

            $check_admin_details = DB::table('users')->where('email' , 'user@ledsend.com')->count();

            if(!$check_admin_details) {

                DB::table('users')->insert([
                    [
                        'name' => 'user',
                        'email' => 'user@ledsend.com',
                        'password' => \Hash::make('123456'),
                        'picture' => envfile('APP_URL')."/placeholder.jpg",
                        'login_by' => 'manual',
                        'mobile' => '9836367763',
                        'device_type' => 'web',
                        'status' => USER_APPROVED,
                        'is_verified' => USER_EMAIL_VERIFIED,
                        'token' => Helper::generate_token(),
                        'token_expiry' => Helper::generate_token_expiry(),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]
                ]);

            }

            $check_test_admin_details = DB::table('users')->where('email' , 'test@ledsend.com')->count();

            if(!$check_test_admin_details) {

                DB::table('users')->insert([
                    [
                        'name' => 'Test',
                        'email' => 'test@ledsend.com',
                        'password' => \Hash::make('123456'),
                        'picture' => envfile('APP_URL')."/placeholder.jpg",
                        'login_by' => 'manual',
                        'mobile' => '9836367763',
                        'device_type' => 'web',
                        'status' => USER_APPROVED,
                        'is_verified' => USER_EMAIL_VERIFIED,
                        'token' => Helper::generate_token(),
                        'token_expiry' => Helper::generate_token_expiry(),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ],
                ]);
            }
        
        }
    }
}
