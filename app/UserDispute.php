<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDispute extends Model
{

protected $hidden = ['deleted_at', 'id', 'unique_id'];

    protected $fillable = ['user_id', 'receiver_user_id', 'user_wallet_payment_id', 'amount', 'message'];

	protected $appends = ['user_dispute_id','user_dispute_unique_id', 'amount_formatted', 'status_formatted'];

    public function getUserDisputeIdAttribute() {

        return $this->id;
    }

    public function getUserDisputeUniqueIdAttribute() {

        return $this->unique_id;
    }

    public function getAmountFormattedAttribute() {

        return formatted_amount($this->amount);
    }

    public function getStatusFormattedAttribute() {

        return dispute_status_formatted($this->status);
    }


    public function DisputeReceiver() {
        return $this->belongsTo('App\User','receiver_user_id');
    }

    public function DisputeSender() {
        return $this->belongsTo('App\User','user_id');
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = "DI-".uniqid();
        });

        static::created(function($model) {

            $model->attributes['unique_id'] = "DI-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });
    }
}
