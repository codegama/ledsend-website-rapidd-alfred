<?php

return [

    101 => 'Successfully loggedin!!',

    102 => 'Mail sent successfully',

    103 => 'Your account deleted.',

    104 => 'Your password changed.',

    106 => 'Logged out',

    105 => 'The card added to your account.',

    108 => 'The card changed to default.',

    109 => 'The card deleted',

    110 => 'The card marked as default.',

    111 => 'The profile updated',

    112 => 'The billing account added',

    113 => 'The billing account deleted',

    114 => 'The documents uploaded and waiting for approval',

    115 => 'The uploaded document deleted',

    116 => 'The uploaded documents are deleted',

    117 => 'The amount added to wallet',

    118 => 'The invoice details updated',

    119 => 'The invoice items updated',

    120 => 'The billing info updated',

    121 => 'The invoice sent',

    122 => 'Sent money successfully.',

    123 => 'The request is cancelled.',

    124 => 'The item is removed',

    125 => 'The dispute is raised successfully',

    126 => 'The dispute is cancelled successfully',

    127 => 'The message sent to admin',

    128 => 'The verification sent to your email address',

    129 => 'Thanks for the email verification.',

    130 => 'The notification preference updated',

    131 => 'The invoice payment done.',

    132 => 'The default account changed',

    133 => 'The amount add request sent to admin',

];
