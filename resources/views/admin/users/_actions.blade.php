<div class="dropdown">

    <button class="btn btn-outline-primary  dropdown-toggle btn-sm" type="button" id="dropdownMenuOutlineButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{tr('action')}}
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuOutlineButton1">

        <a class="dropdown-item" href="{{route('admin.users.view', ['user_id' => $user_details->id])}}">
            {{tr('view')}}
        </a>

        @if(Setting::get('is_demo_control_enabled') == NO)

        <a class="dropdown-item" href="{{route('admin.users.edit', ['user_id' => $user_details->id])}}">
            {{tr('edit')}}
        </a>

        <a class="dropdown-item" href="{{route('admin.users.delete', ['user_id' => $user_details->id])}}" onclick="return confirm(&quot;{{tr('user_delete_confirmation' , $user_details->name)}}&quot;);">
            {{tr('delete')}}
        </a>
        @else

        <a class="dropdown-item" href="javascript:;">{{tr('edit')}}</a>

        <a class="dropdown-item" href="javascript:;">{{tr('delete')}}</a>
        @endif

        <div class="dropdown-divider"></div>

        @if($user_details->is_verified == USER_EMAIL_NOT_VERIFIED)

        <a class="dropdown-item" href="{{route('admin.users.verify', ['user_id' => $user_details->id])}}"> {{tr('verify')}}
        </a>

        @endif


        <a class="dropdown-item" href="{{route('admin.user_billing_accounts.index',['user_id'=>$user_details->id])}}">
            <span class="nav-text">{{tr('billing_accounts')}}</span>

        </a>

        @if($user_details->status == USER_APPROVED)

            <a class="dropdown-item" href="{{route('admin.users.status', ['user_id' => $user_details->id])}}" onclick="return confirm(&quot;{{$user_details->first_name}} - {{tr('user_decline_confirmation')}}&quot;);">
                {{tr('decline')}}
            </a>

        @else

            <a class="dropdown-item" href="{{route('admin.users.status', ['user_id' => $user_details->id])}}">
                {{tr('approve')}}
            </a>

        @endif

    </div>

</div>