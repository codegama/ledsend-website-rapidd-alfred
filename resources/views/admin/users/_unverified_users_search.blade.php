<div class="col-md-12 mb-2 action-btn">

    <form class="col-sm-12 col-sm-offset-6 search_box_css" action="{{route('admin.unverified_users.index')}}" method="GET" role="search">

        <div class="row input-group">

            <div class="col-md-3"></div>

            <div class="col-md-4">
                <input type="text" class="form-control" name="search_key" placeholder="{{tr('user_search_placeholder')}}" value="{{Request::get('search_key')??''}}"> <span class="input-group-btn"></span>
            </div>

            <div class="col-md-2">
                <select class="form-control" name="status">
                    <option value="">{{tr('select_status')}}</option>
                    <option value="{{USER_APPROVED}}" @if(Request::get('status')==USER_APPROVED && Request::get('status')!='') selected @endif>{{tr('approved')}}</option>
                    <option value="{{USER_PENDING}}" @if(Request::get('status')==USER_PENDING && Request::get('status')!='') selected @endif>{{tr('declined')}}</option>
                </select>
            </div>


            <div class="col-md-3">
                <button type="submit" class="btn btn-info">
                    <span class="glyphicon glyphicon-search"> {{tr('search')}}</span>
                </button>
                <a class="btn btn-danger" href="{{route('admin.unverified_users.index')}}">{{tr('clear')}}</a>
                </span>
            </div>
            
        </div>

    </form>

</div>