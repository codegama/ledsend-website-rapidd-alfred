<div class="dropdown">

    <button class="btn btn-outline-primary  dropdown-toggle btn-sm" type="button" id="dropdownMenuOutlineButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{tr('action')}}
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuOutlineButton1">

        <a class="dropdown-item" href="{{ route('admin.user_withdrawals.view', ['user_withdrawal_id' => $withdrawal_details->id]) }}">
            {{tr('view')}}
        </a>

       
        @if($withdrawal_details->status == WITHDRAW_INITIATED)
        <a class="dropdown-item" href="javascript:void(0)" data-toggle="modal" data-target="#paynowModal{{$i}}">
            <span class="nav-text">{{tr('paynow')}}</span>

        </a>
        @endif

        @if($withdrawal_details->status == WITHDRAW_PAID)

            <a class="dropdown-item" href="{{ route('admin.user_withdrawals.decline', ['user_withdrawal_id' => $withdrawal_details->id]) }}" onclick="return confirm(&quot;{{tr('user_withdrawal_decline_confirmation')}}&quot;);">
                {{tr('decline')}}
            </a>

        @endif

    </div>

</div>