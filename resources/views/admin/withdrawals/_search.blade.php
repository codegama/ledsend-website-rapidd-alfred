<div class="col-md-12 mb-2">

    <form class="col-sm-12 col-sm-offset-6 search_box_css" action="{{route('admin.user_withdrawals.index')}}" method="GET" role="search">

        <div class="input-group">

            <div class="col-md-1"></div>

            <div class="col-md-4">
                <input type="text" class="form-control" name="search_key" placeholder="{{tr('user_search_placeholder')}}" value="{{Request::get('search_key')??''}}"> <span class="input-group-btn"></span>
            </div>

            <div class="col-md-3">
                <select class="form-control" name="status">
                    <option value="">{{tr('select_status')}}</option>
                    <option value="{{WITHDRAW_INITIATED}}" @if(Request::get('status')== WITHDRAW_INITIATED && Request::get('status')!='' ) selected @endif>{{tr('requested')}}</option>
                    <option value="{{WITHDRAW_PAID}}" @if(Request::get('status')== WITHDRAW_PAID && Request::get('status')!='' ) selected @endif>{{tr('paid')}}</option>
                    <option value="{{WITHDRAW_ONHOLD}}" @if(Request::get('status')== WITHDRAW_ONHOLD && Request::get('status')!='' ) selected @endif>{{tr('on_hold')}}</option>
                    <option value="{{WITHDRAW_DECLINED}}" @if(Request::get('status')== WITHDRAW_DECLINED && Request::get('status')!='' ) selected @endif>{{tr('declined')}}</option>

                </select>
            </div>

            <div class="col-md-3">
                <button type="submit" class="btn btn-info">
                    <span class="glyphicon glyphicon-search"> {{tr('search')}}</span>
                </button>

                <a class="btn btn-danger" href="{{route('admin.user_withdrawals.index')}}">{{tr('clear')}}</a>
            </div>
        </div>

    </form>

</div>