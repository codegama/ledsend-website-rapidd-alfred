

            <div class="card-body">

                @if(Setting::get('is_demo_control_enabled') == NO)

                <form class="forms-sample" action="{{ route('admin.kyc_documents.save') }}" method="POST" enctype="multipart/form-data" role="form">

                    @else

                    <form class="forms-sample" role="form">

                        @endif

                        @csrf

                        <div class="form-body">

                            <input type="hidden" name="kyc_document_id" id="kyc_document_id" value="{{$kyc_document_details->id}}">

                            <div class="row p-t-20">


                                <div class="form-group col-md-12">
                                    <label for="name">{{ tr('name') }} <span class="admin-required">*</span></label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="{{ tr('name') }}" value="{{ old('name') ?: $kyc_document_details->name}}" required>
                                </div>

                            </div>

                            <div class="row">
                            
                                <div class="form-group col-md-6">
                                <label for="name">{{ tr('upload_preview_image') }} <span class="admin-required">*</span></label>
                                <span class="file_input_format_css"> {{tr('upload_files')}}</span> 
                                <div class="input-group col-xs-12">

                                        <input type="file" name="picture" class="form-control file-upload-info" placeholder="{{tr('upload_preview_image')}}" accept="image/*">

                                        <div class="input-group-append">
                                            <button class="btn btn-info" type="button">{{tr('upload')}}</button>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">

                                <div class="form-group col-md-12">

                                    <label for="simpleMde">{{ tr('description') }}</label>

                                    <textarea class="form-control" id="description" name="description">{{ old('description') ?: $kyc_document_details->description}}</textarea>

                                </div>

                            </div>

                        </div>

                        <div class="form-actions">

                            @if(Setting::get('is_demo_control_enabled') == NO )

                            <button type="submit" class="btn btn-primary btn-pill pull-right"> <i class="fa fa-check"></i>{{tr('save')}}</button>

                            @else

                            <button type="button" class="btn btn-primary btn-pill pull-right"> <i class="fa fa-check"></i>{{tr('save')}}</button>

                            @endif

                            <button type="reset" class="btn reset-btn btn-pill">{{tr('reset')}}</button>

                        </div>

                    </form>

            </div>

       